import Head from "next/head";
import Link from 'next/link'
import Layout from "../components/Layout";

const contact = () => {
    return (
        <Layout>
            <div>
                {/* Hello world */}
                <div data-collapse="medium" data-animation="over-right" data-duration={400} role="banner" className="navbar-1 w-nav">
                    <div className="navbar-1-container">
                        <Link href='/'>
                            <a className="w-nav-brand"><img src="images/Frame.png" width={130} sizes="130px" srcSet="images/Frame-p-500.png 500w, images/Frame.png 635w" alt="" /></a>
                        </Link>
                        <nav role="navigation" className="nav-menu-2 w-nav-menu">
                            <a href="#" className="navbar-1-link w-nav-link">Pricing</a>
                            <a href="#" className="navbar-1-link w-nav-link">How it Works</a>
                            <Link href='/contact/'>
                                <a aria-current="page" className="navbar-1-link w-nav-link w--current">Contact</a>
                            </Link>
                            <a href="#" className="navbar-1-button login w-button">Sign In</a>
                            <a href="#" className="navbar-1-button signup w-button">Become a Merchant</a>
                        </nav>
                        <div className="menu-button w-nav-button">
                            <div className="icon-2 w-icon-nav-menu" />
                        </div>
                    </div>
                </div>
                <div className="contact-form-wrapper">
                    <section id="contact-form" className="contact-form">
                        <div className="w-container">
                            <h2 className="form-title-cf2">Hi there !</h2>
                            <p className="form-subtitle-cf2">How can we help you?</p>
                            <div className="w-form">
                                <form id="email-form" name="email-form" data-name="Email Form" className="form-cf2"><input type="text" className="text-field-cf2 w-input" maxLength={256} name="name-2" data-name="Name 2" placeholder="Name" id="name-2" required /><input type="email" className="text-field-cf2 w-input" maxLength={256} name="Email-2" data-name="Email 2" placeholder="Email" id="Email-2" required /><textarea placeholder="Message" maxLength={5000} id="field" name="field" data-name="Field" className="text-field-cf2 big w-input" defaultValue={""} /><input type="submit" defaultValue="Send Message =>" data-wait="Please wait..." className="button-standard w-button" /></form>
                                <div className="success-message-cf2 w-form-done">
                                    <div>Thank you! Your submission has been received!</div>
                                </div>
                                <div className="error-message-cf2 w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form. <br />Please refresh and try again.</div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="footer-3">
                    <div className="footer-2-container-2">
                        <div className="row-9 w-row">
                            <div className="footer-2-column-2 w-col w-col-6"><img src="images/Frame-1.png" width={120} alt="" className="image-12" />
                                <div className="social-link-wrap">
                                    <div className="footer-2-link copy">Copyright 2020 - Amader Company</div>
                                </div>
                            </div>
                            <div className="footer-2-column-2 w-col w-col-2">
                                <div className="footer-2-header">Navigate</div>
                                <Link href='/'>
                                    <a className="footer-2-link-2">Home</a>
                                </Link>
                                <a href="#" className="footer-2-link-2">Pricing</a>
                                <a href="#" className="footer-2-link-2">How it works</a>
                                <Link href='/contact/'>
                                    <a aria-current="page" className="footer-2-link-2 w--current">Contact</a>
                                </Link>
                            </div>
                            <div className="footer-2-column-2 w-col w-col-2">
                                <div className="footer-2-header-2">Socials</div>
                                <a href="#" className="footer-2-link-2">Instagram</a>
                                <a href="#" className="footer-2-link-2">Facebook</a>
                                <a href="#" className="footer-2-link-2">LinkedIn</a>
                            </div>
                            <div className="footer-2-column-2 w-col w-col-2">
                                <div className="footer-2-header-2">Legal</div>
                                <a href="#" className="footer-2-link-2">Privacy Policy</a>
                                <a href="#" className="footer-2-link-2">Terms of Use</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}

export default contact;