import Link from 'next/link'
import Layout from '../components/Layout'

export default function Home() {
  return (
    <Layout>
      <div>
        {/* Hello world */}
        <div data-collapse="medium" data-animation="over-right" data-duration={400} role="banner" className="navbar-1 w-nav">
          <div className="navbar-1-container">
            <Link href='/'>
              <a aria-current="page" className="w-nav-brand w--current"><img src="images/Frame.png" width={130} sizes="130px" srcSet="images/Frame-p-500.png 500w, images/Frame.png 635w" alt="" /></a>
            </Link>
            <nav role="navigation" className="nav-menu-2 w-nav-menu">
              <a href="#pricing" className="navbar-1-link w-nav-link">Pricing</a>
              <a href="#Demo" className="navbar-1-link w-nav-link">How it Works</a>
              <Link href='/contact/'>
                <a className="navbar-1-link w-nav-link">Contact</a>
              </Link>
              <a href="#" className="navbar-1-button login w-button">Sign In</a>
              <a href="#" className="navbar-1-button signup w-button">Become a Merchant</a>
            </nav>
            <div className="menu-button w-nav-button">
              <div className="icon-2 w-icon-nav-menu" />
            </div>
          </div>
        </div>
        <div>
          <div id="Hero" className="header-1">
            <div className="container-5 w-container">
              <div className="div-block-4">
                <h1 className="header-1-h1">Most Advanced Delivery Network</h1>
                <p className="paragraph header">By collaborating with multiple dleivery services and using machine learning in the process, Amader Delivery has built the most advanced delivery network in the country.<br /></p>
                <a href="#" className="button-standard hero w-button">Become a Merchant</a>
              </div>
            </div>
          </div>
        </div>
        <div className="section social">
          <div className="header-1-container social-proof w-container">
            <p className="paragraph-2">Our Delivery Partners are the Best in the Country</p>
            <div className="w-layout-grid grid">
              <div id="w-node-9d4eca7c7c61-9d036e53"><img src="images/Group-1.png" loading="lazy" alt="" className="brand-logo" /></div>
              <div id="w-node-335088ed4c2a-9d036e53"><img src="images/Group-1.png" loading="lazy" alt="" className="brand-logo" /></div>
              <div id="w-node-810764b70eef-9d036e53"><img src="images/Group-1.png" loading="lazy" alt="" className="brand-logo" /></div>
              <div id="w-node-0fdcea8f986d-9d036e53"><img src="images/Group-1.png" loading="lazy" alt="" className="brand-logo" /></div>
            </div>
          </div>
        </div>
        <div className="section-content-grey">
          <div className="container-3">
            <div className="columns-2 w-row">
              <div className="w-col w-col-6 w-col-stack">
                <h3 className="heading">Most Advanced Delivery Network</h3>
                <p className="paragraph dark">By collaborating with multiple dleivery services and using machine learning in the process, Amader Delivery has built the most advanced delivery network in the country.</p>
              </div>
              <div className="column w-col w-col-6 w-col-stack">
                <div className="feature-wrap">
                  <div className="feature-wrap-left">
                    <div className="tick-circle"><img src="images/Vector-2.png" width={18} alt="" /></div>
                  </div>
                  <div className="feature-wrap-right">
                    <h3 className="h4-2">Nationwide Delivery</h3>
                    <p className="paragraph-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                  </div>
                </div>
                <div className="feature-wrap">
                  <div className="feature-wrap-left">
                    <div className="tick-circle"><img src="images/Vector-2.png" width={18} alt="" /></div>
                  </div>
                  <div className="feature-wrap-right">
                    <h3 className="h4-2">Payment Via Bkash</h3>
                    <p className="paragraph-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                  </div>
                </div>
                <div className="feature-wrap">
                  <div className="feature-wrap-left">
                    <div className="tick-circle"><img src="images/Vector-2.png" width={18} alt="" /></div>
                  </div>
                  <div className="feature-wrap-right">
                    <h3 className="h4-2">Doorstep pickup and delivery</h3>
                    <p className="paragraph-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                  </div>
                </div>
                <div className="feature-wrap">
                  <div className="feature-wrap-left">
                    <div className="tick-circle"><img src="images/Vector-2.png" width={18} alt="" /></div>
                  </div>
                  <div className="feature-wrap-right">
                    <h3 className="h4-2">Fraud Detection</h3>
                    <p className="paragraph-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="section">
          <div className="header-1-container value w-container">
            <div className="w-layout-grid grid-2">
              <div id="w-node-4cf85eb9bc39-9d036e53">
                <div className="div-block-2"><img src="images/Group-4.png" loading="lazy" sizes="143.78125px" srcSet="images/Group-4-p-500.png 500w, images/Group-4.png 821w" alt="" className="image-13" />
                  <h1 className="h4-2 light">Personalized Experience</h1>
                  <p className="paragraph-3 light">We understand that the same delivery service might provide different levels of satisfaction to different merchants for multiple uncontrollable variables. Which is why we take our feedback and use that to tailor-fit your experience to be as satisfactory as possible.<br /></p>
                </div>
              </div>
              <div>
                <div className="div-block-3"><img src="images/Capa-2.png" loading="lazy" alt="" className="image-13" />
                  <h1 className="h4-2 light">Easy to Use</h1>
                  <p className="paragraph-3 light">A dashboard that was built with the intention of making it as easy to use as possible. We strive to make sure your experience of managing deliveries is as satisfactory as stress-free as possible.</p>
                </div>
              </div>
              <div id="w-node-356452e7fefc-9d036e53" className="div-block"><img src="images/Group-3.png" loading="lazy" alt="" className="image-13" />
                <h1 className="h4-2 light">Machine Learning</h1>
                <p className="paragraph-3 light">We use our proprietary machine learning model to determine the right delivery service for your parcel based on it's characteristics. We also provide a data-driven solution to minimize fraudulent.</p>
              </div>
            </div>
          </div>
        </div>
        <div id="Demo" className="section-content-blue">
          <div className="container-flex-vert">
            <h3 className="heading middle">Meet our application.</h3>
            <p className="paragraph dark middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. </p>
            <div data-duration-in={300} data-duration-out={700} className="tabs w-tabs">
              <div className="tabs-menu w-tab-menu">
                <a data-w-tab="Tab 1" className="tab-on w-inline-block w-tab-link w--current">
                  <div className="h4-2 over">Secure &amp; Fast</div>
                  <p className="paragraph dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                </a>
                <a data-w-tab="Tab 2" className="tab-on w-inline-block w-tab-link">
                  <div className="h4-2">Intuitive Interface</div>
                  <p className="paragraph dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                </a>
                <a data-w-tab="Tab 3" className="tab-on w-inline-block w-tab-link">
                  <div className="h4-2">User Friendly Dashboard</div>
                  <p className="paragraph dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim</p>
                </a>
              </div>
              <div className="w-tab-content">
                <div data-w-tab="Tab 1" className="w-tab-pane w--tab-active"><img src="images/phonex.png" sizes="(max-width: 479px) 100vw, (max-width: 767px) 90vw, (max-width: 991px) 550px, 40vw" srcSet="images/phonex-p-500.png 500w, images/phonex-p-800.png 800w, images/phonex-p-1080.png 1080w, images/phonex-p-1600.png 1600w, images/phonex.png 1920w" alt="" className="phone" /></div>
                <div data-w-tab="Tab 2" className="w-tab-pane"><img src="images/phonex.png" sizes="(max-width: 479px) 100vw, (max-width: 767px) 90vw, (max-width: 991px) 550px, 40vw" srcSet="images/phonex-p-500.png 500w, images/phonex-p-800.png 800w, images/phonex-p-1080.png 1080w, images/phonex-p-1600.png 1600w, images/phonex.png 1920w" alt="" className="phone" /></div>
                <div data-w-tab="Tab 3" className="w-tab-pane"><img src="images/phonex.png" sizes="(max-width: 479px) 100vw, (max-width: 767px) 90vw, (max-width: 991px) 550px, 40vw" srcSet="images/phonex-p-500.png 500w, images/phonex-p-800.png 800w, images/phonex-p-1080.png 1080w, images/phonex-p-1600.png 1600w, images/phonex.png 1920w" alt="" className="phone" /></div>
              </div>
            </div>
          </div>
        </div>
        <div id="pricing" className="pricing-section">
          <div className="w-container">
            <h3 className="heading middle over">See your pricing</h3>
          </div>
          <div>
            <div className="w-layout-grid price-grid">
              <div className="pricing-column-features">
                <div className="w-layout-grid feature-grid">
                  <div className="pricing-top override" />
                  <div className="feature-item">
                    <div className="feature-text-2">COD&nbsp;Charge</div>
                  </div>
                  <div className="feature-item">
                    <div className="feature-text-2">Increment per KG</div>
                  </div>
                  <div className="feature-item">
                    <div className="feature-text-2">Delivery Time</div>
                  </div>
                  <div className="feature-item">
                    <div className="feature-text-2">Fraud Detection Service</div>
                  </div>
                  <div className="feature-item">
                    <div className="feature-text-2">Max Weight</div>
                  </div>
                  <div className="feature-item">
                    <div className="feature-text-2">Merchant Pickup</div>
                  </div>
                </div>
              </div>
              <div id="w-node-abc79baa6f6d-9d036e53" className="pricing-column-white">
                <div className="w-layout-grid feature-grid">
                  <div className="pricing-top">
                    <h3 className="price-type-2">Door to Door</h3>
                    <h1>BDT100</h1>
                    <div className="paragraph price">Upto 1KG inside Dhaka</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">COD Charge </div>
                    <div className="feature-text-2">0%</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Increment per KG </div>
                    <div className="feature-text-2">BDT20</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Delivery Time</div>
                    <div className="feature-text-2">4-6 Hours</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Fraud Detection Service</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Max Weight</div>
                    <div className="feature-text-2">15KG</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Merchant Pickup</div>
                  </div>
                </div>
              </div>
              <div id="w-node-abc79baa6f98-9d036e53" className="pricing-column">
                <div className="w-layout-grid feature-grid">
                  <div id="w-node-abc79baa6f9a-9d036e53" className="pricing-top">
                    <h3 className="price-type-2">Inside Dhaka</h3>
                    <h1 className="heading-4">BDT60</h1>
                    <div className="paragraph price">Upto 1KG</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">COD&nbsp;Charge </div>
                    <div className="feature-text-2">0%</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Increment per KG </div>
                    <div className="feature-text-2">BDT20</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Delivery Time</div>
                    <div className="feature-text-2">24-36 Hours</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Fraud Detection Service</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Max Weight</div>
                    <div className="feature-text-2">15KG</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Merchant Pickup</div>
                  </div>
                </div>
              </div>
              <div className="pricing-column">
                <div className="w-layout-grid feature-grid">
                  <div className="pricing-top">
                    <h3 className="price-type-2">Around Dhaka</h3>
                    <h1>BDT100</h1>
                    <div className="paragraph price">Upto 1KG</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">COD Charge </div>
                    <div className="feature-text-2">1%</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Increment per KG </div>
                    <div className="feature-text-2">BDT25</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Delivery Time</div>
                    <div className="feature-text-2">2-3 Days</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Fraud Detection Service</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Max Weight</div>
                    <div className="feature-text-2">15KG</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Merchant Pickup</div>
                  </div>
                </div>
              </div>
              <div className="pricing-column">
                <div className="w-layout-grid feature-grid">
                  <div id="w-node-2c3793cd4d82-9d036e53" className="pricing-top">
                    <h3 className="price-type-2">Outside Dhaka</h3>
                    <h1>BDT130</h1>
                    <div className="paragraph price">Upto 1KG</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">COD Charge </div>
                    <div className="feature-text-2">2%</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Increment per KG </div>
                    <div className="feature-text-2">BDT30</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Delivery Time</div>
                    <div className="feature-text-2">4-7 Days</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Fraud Detection Service</div>
                  </div>
                  <div className="feature-check">
                    <div className="feature-text-mobile">Max Weight</div>
                    <div className="feature-text-2">15KG</div>
                  </div>
                  <div className="feature-check"><img src="images/Vector-2.png" alt="" className="check-3" />
                    <div className="feature-text-mobile">Merchant Pickup</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="pricing-wrap wra">
              <div className="pricing-column-03">
                <div className="pricing-card">
                  <h3 className="pricing-h3">basic</h3>
                  <div className="pricing-details-wrap">
                    <div className="pricing">$12</div>
                    <div className="date">month</div>
                  </div>
                  <div className="pricing-check-wrap">
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Cloneable Content</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Beautiful Templates</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Detailed Guides</p>
                    </div>
                  </div>
                  <a href="#" className="pricing-button w-button">Get Started</a>
                </div>
              </div>
              <div className="pricing-column-03">
                <div className="pricing-card purple">
                  <h3 className="pricing-h3 white">business</h3>
                  <div className="pricing-details-wrap">
                    <div className="pricing white">$32</div>
                    <div className="date white">month</div>
                  </div>
                  <div className="pricing-check-wrap">
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text white">Cloneable Content</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text white">Beautiful Templates</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text white">Detailed Guides</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text white">Expert Support</p>
                    </div>
                  </div>
                  <a href="#" className="pricing-button white w-button">Get Started</a>
                </div>
                <div className="pricing-details">* Billed as $420 yearly<br /></div>
              </div>
              <div className="pricing-column-03">
                <div className="pricing-card">
                  <h3 className="pricing-h3">Professional</h3>
                  <div className="pricing-details-wrap">
                    <div className="pricing">$79</div>
                    <div className="date">month</div>
                  </div>
                  <div className="pricing-check-wrap">
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Cloneable Content</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Beautiful Templates</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Detailed Guides</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Expert Support</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Communities &amp; Groups</p>
                    </div>
                    <div className="pricing-check"><img src="https://uploads-ssl.webflow.com/5aa5deb2f3d89b000123c7dd/5cd24ca168db6560f9e01747_check.svg" alt="" className="check" />
                      <p className="pricing-text">Inspiration &amp; Jobs</p>
                    </div>
                  </div>
                  <a href="#" className="pricing-button w-button">Get Started</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="Testimonial" className="testimonial-section">
          <div className="container-2">
            <div className="title-wrap">
              <h2 className="heading light middle">What they say about us</h2>
              <p className="paragraph social">Create beautiful webflow sites with this flowbase free template.</p>
            </div>
            <div className="testimonial-wrapper">
              <div data-animation="slide" data-duration={500} data-infinite={1} className="slider w-slider">
                <div className="mask w-slider-mask">
                  <div className="slide w-slide">
                    <div className="slide-wrap">
                      <div className="standard-paragraph">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</div>
                      <div className="profile-block"><img src="images/Avatar-1_1Avatar-1.png" width={60} alt="" className="profile-image" />
                        <div className="profile-details">
                          <div className="faq-title">Sarah Smith</div>
                          <div className="author-title">Lead Developer, Amader Website</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slide w-slide">
                    <div className="slide-wrap">
                      <div className="standard-paragraph">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</div>
                      <div className="profile-block"><img src="images/Avatar-2_1Avatar-2.png" width={60} alt="" className="profile-image" />
                        <div className="profile-details">
                          <div className="faq-title">Craig Sams</div>
                          <div className="author-title">Lead Developer, Amader Website</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="slide w-slide">
                    <div className="slide-wrap">
                      <div className="standard-paragraph">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</div>
                      <div className="profile-block"><img src="images/Avatar-3_1Avatar-3.png" width={60} alt="" className="profile-image" />
                        <div className="profile-details">
                          <div className="faq-title">Simon Lee</div>
                          <div className="author-title">Lead Developer, Amader Website</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="arrow-left w-slider-arrow-left">
                  <div className="arrow-icon w-icon-slider-left" />
                </div>
                <div className="arrow-right w-slider-arrow-right">
                  <div className="arrow-icon w-icon-slider-right" />
                </div>
                <div className="slide-nav w-slider-nav w-round" />
              </div>
            </div>
          </div>
        </div>
        <div className="cta-2">
          <div className="cta-2-container">
            <h1 className="heading middle over">Ready to get started?<br /></h1>
            <a href="#" className="button-standard w-button">Become a Merchant</a>
          </div>
        </div>
        <div className="footer-3">
          <div className="footer-2-container-2">
            <div className="row-9 w-row">
              <div className="footer-2-column-2 w-col w-col-6"><img src="images/Frame-1.png" width={120} alt="" className="image-12" />
                <div className="social-link-wrap">
                  <div className="footer-2-link copy">Copyright 2020 - Amader Company</div>
                </div>
              </div>
              <div className="footer-2-column-2 w-col w-col-2">
                <div className="footer-2-header">Navigate</div>
                <Link href='/'>
                  <a aria-current="page" className="footer-2-link-2 w--current">Home</a>
                </Link>
                <a href="#pricing" className="footer-2-link-2">Pricing</a>
                <a href="#Demo" className="footer-2-link-2">How it works</a>
                <Link href='/contact/'>
                  <a className="footer-2-link-2">Contact</a>
                </Link>
              </div>
              <div className="footer-2-column-2 w-col w-col-2">
                <div className="footer-2-header-2">Socials</div>
                <a href="#" className="footer-2-link-2">Instagram</a>
                <a href="#" className="footer-2-link-2">Facebook</a>
                <a href="#" className="footer-2-link-2">LinkedIn</a>
              </div>
              <div className="footer-2-column-2 w-col w-col-2">
                <div className="footer-2-header-2">Legal</div>
                <a href="#" className="footer-2-link-2">Privacy Policy</a>
                <a href="#" className="footer-2-link-2">Terms of Use</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
